/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ComponentAssembler;

import ComponentModel.Component;
import ComponentModel.ComponentStates;

/**
 *
 * @author anton
 */
public class ComponentAssemblerTest {
    
    
    //static String testpath = "/home/anton/Development/NetBeansProjects/Testclasses";
    static String testpath = System.getProperty("user.dir") + "/Components";
    
    
    
    public static void main(String[] args) throws Exception {
        ComponentAssembler assembler = new ComponentAssembler();
        assembler.addPath(testpath);
        String Buchungssystem_jar = "/home/anton/Development/NetBeansProjects/Buchungssystem/dist/Buchungssystem.jar";
        String Buchungssystem_comp = "org.bonn.ooka.Buchungssystem.HotelRetrieval";
        
        //env.deployJAR(Buchungssystem_jar, Buchungssystem_comp);
        //env.deployJAR("/home/anton/Development/NetBeansProjects/Testclasses/JarComponent_1.jar", "Component_1");
        //env.startComponent("org.bonn.ooka.Buchungssystem.HotelRetrieval");
        
        
        //----DEMO-----//
        
        //deploy components
        assembler.deployComponent("Component_0");
        assembler.deployComponent("Component_1");
        
        //let Component_1 observerve Component_0:
        assembler.observeStates("Component_1", "Component_0", ComponentStates.ALL);
        assembler.startComponent("Component_1");
        
        //list status of all components
        System.out.println(assembler.listDeployedComponents());
        
        //start Component_0. -> Component_1 will be notified!
        assembler.startComponent("Component_0");
        
        //let Component_1 work for a bit
        Thread.sleep(1000);
        
        //stop Component_0. Component_1 will be notified again.
        assembler.stopComponent("Component_0");
        
        
        //Composition:
        //inject Component_0 into Component_1:
        assembler.composeComponents("Component_1", "Component_0");
        System.out.println(assembler.listDeployedComponents());
        
        //update lifecycle of Component_0
        assembler.updateLifecycle("Component_0", Component.Lifecycle.IN_MAINTENANCE);
        Thread.sleep(1000);
        assembler.updateLifecycle("Component_0", Component.Lifecycle.UNDER_TEST);
        
        assembler.removeComponent("Component_1");
        
        
        System.out.println(assembler.listDeployedComponents());
        
        
        //assembler.startComponent(Buchungssystem_comp);
        
        assembler.exit();
    }
    
}
