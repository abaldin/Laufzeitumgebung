import ComponentModel.*;

public class Component_1 {

	private boolean running;
	private Component composition;

	public Component_1() {
		running = false;	
	}

	static {
		System.out.println("Class Component_1 loaded!");
	}

	@ComponentCompose
	public void setComponent(Component c) {
		this.composition = c;
		if(this.composition != null)
			System.out.println("Component " + this.composition.getName() + " injected!");	
		else 
			System.out.println("Component removed!");	
	}	

	@ComponentStart
	public void  startComponent() {
		System.out.println("Component_1 started!");
		
		//wait for other component to start.
		while(!running) try { Thread.sleep(100); } catch (InterruptedException e){}

		int i = 0;
		while(running) {

			System.out.println("Component_1 working: " + i++);
			try { Thread.sleep(100); } catch (InterruptedException e){
				System.out.println("Component_1 interrupted!");}
		}
	}

	@ComponentStop
	public void stopComponent() {
		System.out.println("Component_1 stopped!");
	}

	@ComponentNotify
	public void bla(ComponentStates state) {
		System.out.println("Component_1 was notified from " + state.componentName + " on event " + state + "!");
		if(state == ComponentStates.STARTED) setRunning(true);
		if(state == ComponentStates.STOPPED) setRunning(false);
	}

	public synchronized void setRunning(boolean value) {
		running = value;
	}

}
