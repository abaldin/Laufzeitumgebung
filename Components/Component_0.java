import ComponentModel.*;

public class Component_0 {

	static {
		System.out.println("Class Component_0 loaded!");
	}

	@ComponentStart
	public void  printStuff() {
		System.out.println("Component_0 started!");
	}

	@ComponentStop
	public void stopComponent() {
		System.out.println("Component_0 stopped!");
	}

	@ComponentNotify
	public void bla(ComponentStates state) {
		System.out.println("Component_0 was notified from " + state.componentName + " on event " + state + "!");
	}

}
