package ComponentAssembler;


import ComponentModel.Component;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author anton
 */
public class ComponentLoader {
    
    private ArrayList<URL> paths;
    
    
    public ComponentLoader() {
        paths = new ArrayList<>();
        //default path for components!
        addPath(System.getProperty("user.dir"));
    }
    
    public void addPath(String path) {
        try {
            paths.add(new File(path).toURI().toURL());
        } catch (MalformedURLException ex) {
            System.err.println("URL is malformed: " + path);
        }
    }
    
    public Component<?> loadComponentFromJar(String jar, String componentName) {
        
        try {
            URLClassLoader child = new URLClassLoader(new URL[]{new File(jar).toURI().toURL()});
            Class classToLoad = Class.forName(componentName, false, child);
            return new Component(classToLoad);
        } catch (MalformedURLException | ClassNotFoundException ex) {
            Logger.getLogger(ComponentLoader.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public Component<?> loadComponent(String componentName) {
        //provide an array of urls from which to load classes
        URLClassLoader cl = new URLClassLoader(paths.toArray(new URL[1]));

        Class<?> c;
        try {
            c = cl.loadClass(componentName);
            return new Component<>(c);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ComponentLoader.class.getName()).log(Level.SEVERE, "Class not found!", ex);
            return null;
        }

    }
    
}
