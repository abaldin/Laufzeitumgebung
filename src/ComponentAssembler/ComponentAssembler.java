/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ComponentAssembler;

import ComponentModel.Component;
import ComponentModel.ComponentStates;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 *
 * @author anton
 */
public class ComponentAssembler {
    
    
    private HashMap<String, Component<?>> components;
    private ComponentLoader loader;
    
    public ComponentAssembler() {
        components = new HashMap<>();
        loader = new ComponentLoader();
    }
    
    public void addPath(String path) {
        loader.addPath(path);
    }
    
    /**
     * deploys a new component with the specified name.
     * @param name 
     */
    public void deployComponent(String name) {
        Component comp = loader.loadComponent(name);
        components.put(comp.getName(), comp);
    }
    
    public void deployJAR(String jar, String name) {
        Component comp = loader.loadComponentFromJar(jar, name);
        components.put(comp.getName(), comp);
        Logger.getLogger(ComponentAssembler.class.getName()).log(Level.INFO, "{0} deployed!", name);
    }
    
    /**
     * removes a component from the runtime environment.
     * if the component is still running, it will be stopped.
     * @param name 
     */
    public void removeComponent(String name) {
        if(components.containsKey(name)) {
            // delete all injected references
            components.get(name).updateLifecycle(Component.Lifecycle.REMOVED);
            Component removed = components.remove(name);
            
            //remove from all observer and injected lists
            components.values().stream().forEach((c) -> {
                c.removeObserver(removed, ComponentStates.ALL);
                c.removeFromInjectionList(removed);
            });
        }
    }
    
    
    public void startComponent(String name) {
        if(components.containsKey(name)) {
            components.get(name).start();
        } else Logger.getLogger(ComponentAssembler.class.getName()).log(Level.WARNING, name + " not deployed!");
    }
    public void stopComponent(String name) {
        if(components.containsKey(name)) {
            components.get(name).stop();
        } else Logger.getLogger(ComponentAssembler.class.getName()).log(Level.WARNING, name + " not deployed!");
    }
    
    /**
     * Connects two components. The first component observes the second one
     * and gets notified on the given state changes.
     * @param observer
     * @param publisher
     * @param states 
     */
    public void observeStates(String observer, String publisher, EnumSet<ComponentStates> states) {
        Component c_observer = components.get(observer);
        Component c_publisher = components.get(publisher);
        
        if(c_observer != null && c_publisher != null) {
            c_publisher.addObserver(c_observer, states);
        }
        Logger.getLogger(ComponentAssembler.class.getName()).log(Level.INFO, "{0} now observes {1}{2}", new Object[]{observer, publisher, states});
    }
    // for convenience
    public void observeStates(String observer, String publisher, ComponentStates state) {
        observeStates(observer, publisher, EnumSet.of(state));
    }
    
    public void composeComponents(String c1, String c2) {
        Component component_1 = components.get(c1);
        Component component_2 = components.get(c2);
        if(component_1 != null && component_2 != null) {
            component_2.addToInjectionList(component_1);
        }
    }
    
    public void updateLifecycle(String component, Component.Lifecycle cycle) {
        Component c = components.get(component);
        if(c != null) {
            c.updateLifecycle(cycle);
        } else Logger.getLogger(ComponentAssembler.class.getName()).log(Level.WARNING, "{0} not deployed!", component);
    }
    
   
    /**
     * lists all deployed components.
     * information shown for each component:
     *  - name
     *  - ID
     *  - status
     * @return String
     */
    public String listDeployedComponents() {
        //for(Component c : components.values()) {
        //    System.out.println(c);
       // }
       StringBuilder sb = new StringBuilder();
       components.values().stream().forEach((c) -> {
           sb.append(c.getStatus());
        });
       return sb.toString();
    }
    
    /**
     * stops the runtime environment.
     * stops all running components
     * removes all deployed components
     */
    public void exit() {
        for(Component comp : components.values()) {
            comp.stop();
        }
        components.clear();
    }
    
}
