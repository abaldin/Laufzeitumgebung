/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ComponentModel;

import java.util.EnumSet;

/**
 *
 * @author anton
 */
public enum ComponentStates {
    STARTED, STOPPED;
    public static final EnumSet<ComponentStates> ALL = EnumSet.allOf(ComponentStates.class);
    
    public String componentName = "";
}
