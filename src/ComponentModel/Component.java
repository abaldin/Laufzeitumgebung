/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ComponentModel;

import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anton
 * @param <T>
 */

public class Component<T> {
    //each component gets a uniqe id
    private static int id_counter = 0;
    private static int generateID() {
        return id_counter++;
    }
    //each component has a lifecycle
    public enum Lifecycle {
        UNDER_TEST, IN_PRODUCTION, UNDER_INSPECTION, IN_MAINTENANCE, REMOVED
    }
    
    private int ID;
    private Lifecycle cycle;
    private EnumSet<Lifecycle> injectionScope; //scope, in which injection is possible
    private Method startMethod, stopMethod, notifyMethod, composeMethod;
    private boolean running;
    
    private Class<T> componentClass;
    private T instance;
    
    //list of all observers of this component, together with the observed events
    private Map<Component, EnumSet<ComponentStates>> observers;
    //private List<Pair<Component,EnumSet<ComponentStates>>> observers;
    //list of all components which have a reference to this.
    private List<Component> injectionList;
    
    private Thread worker;
    
    public Component(Class<T> componentClass) {
        this.componentClass = componentClass;
        this.ID = generateID();
        this.cycle = Lifecycle.UNDER_TEST;
        this.injectionScope = EnumSet.of(Lifecycle.UNDER_TEST, Lifecycle.UNDER_INSPECTION);
        this.running = false;
        
        this.observers = new HashMap<>();
        this.injectionList = new ArrayList<>();
        
        //find the annotated start and stop methods:
        for(Method m : componentClass.getDeclaredMethods()) {
            if(m.isAnnotationPresent(ComponentStart.class)) 
                startMethod = m;
            if(m.isAnnotationPresent(ComponentStop.class))
                stopMethod = m;
            if(m.isAnnotationPresent(ComponentNotify.class))
                notifyMethod = m;
            if(m.isAnnotationPresent(ComponentCompose.class))
                composeMethod = m;
        }
        
        try {
            instance = componentClass.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(Component.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public String getName() {
        return componentClass.getName();
    }
    public int getID() {
        return ID;
    }
    
    public void start() {
        if (!running) {
            
            worker = new Thread(() -> {
                try {
                    startMethod.invoke(instance);
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    Logger.getLogger(Component.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            worker.start();

            running = true;
            Logger.getLogger(Component.class.getName()).log(Level.INFO, "{0} started!", this.getName());
        }
        //notify observers
        notifyObservers(ComponentStates.STARTED);
    }
    
    public void stop() {
        if(running) {
            worker.interrupt();
            
            worker = new Thread(() -> {
                try {
                    stopMethod.invoke(instance);
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    Logger.getLogger(Component.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            worker.start();
            
            running = false;
            Logger.getLogger(Component.class.getName()).log(Level.INFO, "{0} stopped!", this.getName());
            //notify observers
            notifyObservers(ComponentStates.STOPPED);
        }

    }
    
    //------------------------OBSERVER FUNCTIONALITY--------------------------//
    //------------------------------------------------------------------------//
    
    public void addObserver(Component observer, EnumSet<ComponentStates> states) {
        observers.put(observer, states);
        //observers.add(new Pair(observer, states));
    }
    public void removeObserver(Component observer, EnumSet<ComponentStates> states) {
        if(observers.containsKey(observer)) {
            observers.get(observer).removeAll(states);
            if(observers.get(observer).isEmpty()) {
                //nothing left to observe!
                observers.remove(observer);
            }
        }
    }
    
    /**
     * Notifies all observers that are registered to a specific state change.
     * @param state 
     */
    public void notifyObservers(ComponentStates state) {
        observers.entrySet().stream().filter((p) -> (p.getValue().contains(state))).forEach((p) -> {
            state.componentName = this.getName();
            p.getKey().invokeNotifyMethod(state);
        });
    }
    
    public void invokeNotifyMethod(ComponentStates state) {
            if(notifyMethod == null) {
                Logger.getLogger(Component.class.getName()).log(Level.WARNING, "{0} has no notify annotated method!", this.getName());
                return;
            }
            
            try {
                notifyMethod.invoke(instance, state);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                Logger.getLogger(Component.class.getName()).log(Level.SEVERE, null, ex);
            }
            Logger.getLogger(Component.class.getName()).log(Level.INFO, "{0} notified!", this.getName());
    }
    //------------------------------------------------------------------------//

    //------------------------INJECTION LOGIC---------------------------------//
    //------------------------------------------------------------------------//
    
    /**
     * add a component in which THIS component should be injected
     * @param c the component
     */
    public void addToInjectionList(Component c) {
        injectionList.add(c);
        if(injectionScope.contains(this.cycle)) {
            //inject right away
            c.invokeComposeMethod(this);
        }
    }
    //called when the component holding the injected reference was deleted
    public void removeFromInjectionList(Component c) {
        injectionList.remove(c);
    }
    
    public void updateLifecycle(Lifecycle lc) {
        if(lc != this.cycle) {  //ignore if no change
            this.cycle = lc;
            
            Logger.getLogger(Component.class.getName()).log(Level.INFO, "Lifecycle of {0} changed to {1}", new Object[]{this.getName(), lc});
            
            //perform injection
            if(!injectionScope.contains(this.cycle)) {
                //remove all references of this component!
                Logger.getLogger(Component.class.getName()).log(Level.WARNING, "previously injected references of {0} will be removed!", this.getName());
                injectionList.stream().forEach((c) -> c.invokeComposeMethod(null));
            } else {
                injectionList.stream().forEach((c) -> c.invokeComposeMethod(this));
            }
            
            
            
        }
    }
    
    /**
     * Perfroms dependency injection: Component toInject gets injected
     * into THIS component (logic happens in the compose annotated mehtod).
     * @param toInject 
     */
    public void invokeComposeMethod(Component toInject) {
        //if(running) {
            if(composeMethod == null) {
                Logger.getLogger(Component.class.getName()).log(Level.WARNING, "{0} has no compose annotated method!", this.getName());
                return;
            }
            
            try {
                composeMethod.invoke(instance, toInject);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                Logger.getLogger(Component.class.getName()).log(Level.SEVERE, null, ex);
            }
            String injName = toInject == null ? "null" : toInject.getName();
            Logger.getLogger(Component.class.getName()).log(Level.INFO, "successfully injected {0} into {1}", new Object[]{injName, this.getName()});
        //}
    }
    
    @Override
    public String toString() {
        return this.getName();
    }
    
    public String getStatus() {
        return  "\nName......." + getName() +
                "\nID........." + ID +
                "\nStatus....." + (running ? "started" : "stopped") +
                "\nObservers.." + observers.toString() +
                "\nInjectedIn." + injectionList.toString() +
                "\n";
    }
    
}
